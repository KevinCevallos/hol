/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerpoo;

/**
 *
 * @author CltControl
 */
public class bus {
     private String placa;
    private String ruta;
    private String color;

    public bus(String placa, String ruta, String color) {
        this.placa = placa;
        this.ruta = ruta;
        this.color = color;
    }

    public String getPlaca() {
        return placa;
    }

    public String getRuta() {
        return ruta;
    }

    public String getColor() {
        return color;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public void setColor(String color12) {
        this.color = color12;
    }
}
